﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.PortableExecutable;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PariwisataWamena.Controllers {
    [Route ("api/[controller]")]
    public class ImageController : ControllerBase {
        public static IHostingEnvironment _environment;
        public ImageController (IHostingEnvironment environment) {
            _environment = environment;
        }

        public class FIleUploadAPI {
            public FIleUploadAPI (IFormFile files) {
                this.files = files;

            }
            public IFormFile files { get; set; }
        }

        [HttpPost ("[action]"), DisableRequestSizeLimit]
        public object Upload () {
            try {
                var file = Request.Form.Files[0];
                var path = _environment.WebRootPath + "/uploads";
                if (!Directory.Exists (path)) {
                    Directory.CreateDirectory (path);
                }
                using (FileStream filestream = System.IO.File.Create (path + "/" + file.FileName)) {
                    file.CopyTo (filestream);
                    filestream.Flush ();

                }

                var result = new { imageUrl = $"\\uploads\\{file.FileName}", ok = true };

                return result;

            } catch (Exception ex) {
                return BadRequest (ex.ToString ());
            }
        }

        [Authorize]
        [HttpPost ("[action]"), DisableRequestSizeLimit]
        public object avatar () {
            try {
                var id = Convert.ToInt32 (User.Identity.Name);
                var file = Request.Form.Files[0];
                var fileName = string.Format ("tourist{0:D4}.jpg", id);
                var path = _environment.WebRootPath + "/avatars";
                if (!Directory.Exists (path)) {
                    Directory.CreateDirectory (path);
                }
                using (FileStream filestream = System.IO.File.Create (path + "/" + fileName)) {
                    file.CopyTo (filestream);
                    filestream.Flush ();
                }

                var result = new { imageUrl = $"\\avatars\\{fileName}", ok = true };

                return result;

            } catch (Exception ex) {
                return BadRequest (ex.ToString ());
            }
        }

        [HttpPost ("[action]"), DisableRequestSizeLimit]
        public object UploadPayment (int id) {
            try {
                var file = Request.Form.Files[0];
                var path = _environment.WebRootPath + "/uploads/payments";
                if (!Directory.Exists (path)) {
                    Directory.CreateDirectory (path);
                }

                var fileName = $"{id:d5}.jpg";
                using (FileStream filestream = System.IO.File.Create (path + "/" + fileName)) {
                    file.CopyTo (filestream);
                    filestream.Flush ();
                }

                var result = new { imageUrl = $"\\uploads\\payments\\{fileName}", ok = true };

                return result;

            } catch (Exception ex) {
                return BadRequest (ex.ToString ());
            }
        }

        [HttpGet ("[action]")]
        public object GetUploadedFile () {
            try {
                var path = _environment.WebRootPath + "/uploads";
                var result = Directory.GetFiles (path, "*.*");
                return result;

            } catch (Exception ex) {
                return BadRequest (ex.ToString ());
            }
        }

        [HttpGet ("[action]")]
        public IActionResult DeleteFile (string filename) {
            try {
                 var path = _environment.WebRootPath + "/uploads";
                FileInfo userphoto = new FileInfo (path+filename);
                if (userphoto.Exists) {
                    //File.Delete($"{path}{filename}");
                }

                return Ok();

            } catch (Exception ex) {
                return BadRequest (ex.ToString ());
            }
        }

    }

}