using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PariwisataWamena.DataAccess;
using PariwisataWamena.Models;

namespace PariwisataWamena.Controllers {

    [ApiController]
    [Route ("api/[controller]")]
    public class StatisticController : Controller {
        StatisticDataAccess context = new StatisticDataAccess ();

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get () {
            try {
                var result = await context.Get();
                return Ok (result);
            } catch (System.Exception ex) {
                return BadRequest (new { message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post ([FromBody] string model) {
            try {
                    var data = new statistic();
                    data.IpAddress=model;
                    data.Tanggal=DateTime.Now;
                var result = await context.Post (data);
                return Ok (result);

            } catch (System.Exception ex) {

                return BadRequest (ex.Message);
            }
        }
    }
}