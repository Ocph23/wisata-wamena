﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PariwisataWamena.DataAccess;
using PariwisataWamena.Models;
using PariwisataWamena.Services;

namespace PariwisataWamena.Controllers {
    [Route ("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ServiceController : ControllerBase {

        [AllowAnonymous]
        // GET: api/Service
        [HttpGet]
        public async Task<IActionResult> get () {
            try {
                var userid = Convert.ToInt32 (User.Identity.Name);
                if (userid > 0) {
                    var agentDa = new AgentDataAccess ();
                    var agent = await agentDa.GetByUserId (userid);
                    var idagent = agent.idagent;
                    var context = new LayananDataAccess (idagent);
                    var result = await context.GetLayanan ();
                    return Ok (result);
                } else
                    return this.Unauthorized ();
            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }
        }

        [AllowAnonymous]
        // GET: api/Service
        [HttpGet ("all")]
        public async Task<IActionResult> getAll () {
            try {
                var context = new LayananDataAccess ();
                var result = await context.GetLayananAll ();
                return Ok (result);
            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> post (layanan data) {
            try {
                var userid = Convert.ToInt32 (User.Identity.Name);
                if (userid > 0) {
                    var agentDa = new AgentDataAccess ();
                    var agent = await agentDa.GetByUserId (userid);
                    data.idagent = agent.idagent;
                    var context = new LayananDataAccess (agent.idagent);
                    var result = await context.CretaLayanan (data);
                    return Ok (result);
                } else
                    return this.Unauthorized ();

            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put (int id, layanan data) {
            try {
                var userid = Convert.ToInt32 (User.Identity.Name);
                if (userid > 0) {
                    var agentDa = new AgentDataAccess ();
                    var agent = await agentDa.GetByUserId (userid);
                    data.idagent = agent.idagent;
                    var context = new LayananDataAccess (id);
                    var result = await context.UpdateLayanan (data);
                    return Ok (result);
                } else
                    return this.Unauthorized ();

            } catch (Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        //
        [HttpGet ("{id}/{serviceId}")]
        public async Task<IActionResult> GetTransactionByServiceId (int id, int serviceId) {
            try {
                var context = new LayananDataAccess (id);
                var result = await context.GetTransaction (serviceId);
                return Ok (result);
            } catch (Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        [HttpGet ("transaction/{id}")]
        public async Task<IActionResult> GetTransactionByAgentId (int id) {
            try {
                var context = new LayananDataAccess (id);
                var result = await context.GetTransactionByAgentId (id);
                return Ok (result);
            } catch (Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        [HttpGet ("mytransaction/{id}")]
        public async Task<IActionResult> GetTransactionByTouristId (int id) {
            try {
                var context = new LayananDataAccess (id);
                var result = await context.GetTransactionByTouristId (id);
                return Ok (result);
            } catch (Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        [HttpPost ("confirmtransaction")]
        public async Task<IActionResult> ConfirmTransaction (transaction model) {
            try {
                var context = new LayananDataAccess ();

                var result = await context.ConfirmTransaction (model);
                if (result) {
                    string emailText = $"Pesanan Anda Dengan Kode : {model.idtransaction:D5} Telah di Verifikasi";
                    var emailService = new EmailServices ();
                    await emailService.SendAsync (new IdentityMessage {
                        Destination = model.tourist.email,
                            CC = model.agent.email,
                            Body = emailText,
                            Subject = "Verifikasi Pembayaran",
                    });
                }

                return Ok (result);
            } catch (Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        [HttpPost ("transcation/create")]
        public async Task<IActionResult> CreateNewTransaction (transaction model) {
            try {
                var context = new LayananDataAccess ();
                var result = await context.CreateNewTransaction (model);
                return Ok (result);
            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }
        }

        [HttpPost ("payment")]
        public async Task<IActionResult> CreatePayment (payment model) {
            try {
                var context = new LayananDataAccess ();
                var result = await context.CreatePayment (model);
                if (result != null) {
                    var agentData = context.GetAgentEmailByTransactionId (model.idtransaction);
                    if (agentData != null) {
                        string emailText = $"Telah Dilakukan Pembayaran Ke {model.bank} sebesar {model.amount} untuk transaksi No {model.idtransaction}";
                        var emailService = new EmailServices ();
                        var touristData = context.GetTouristEmailByTransactionId (model.idtransaction);
                        var touristEmail = "";
                        if (touristData != null)
                            touristEmail = touristData.email;

                        var email = new IdentityMessage {
                            Destination = agentData.email,
                            Body = emailText,
                            CC = touristEmail,
                            Subject = "Pembayaran"
                        };
                        await emailService.SendAsync (email);
                    }
                    return Ok (result);
                }
                throw new SystemException ("Data Tidak tersimpan");
            } catch (Exception ex) {
                return BadRequest (ex.Message);
            }
        }

    }
}