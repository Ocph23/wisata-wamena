using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PariwisataWamena.Helpers;
using PariwisataWamena.Models;
using PariwisataWamena.Services;

namespace PariwisataWamena.Controllers {
    [Authorize]
    [ApiController]
    [Route ("[controller]")]
    public class AccountController : ControllerBase {
        private IUserService _userService;
        //  private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AccountController (IUserService userService,
            IOptions<AppSettings> appSettings) {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost ("authenticate")]
        public async Task<IActionResult> Authenticate ([FromBody] User userDto) {
            try {
                Console.WriteLine (userDto.username + " Ok");
                var user = await _userService.Authenticate (userDto.username, userDto.password);

                if (user == null)
                    throw new SystemException ("Username or password is incorrect");

                var tokenHandler = new JwtSecurityTokenHandler ();
                var key = Encoding.ASCII.GetBytes (_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor {
                    Subject = new ClaimsIdentity (new Claim[] {
                    new Claim (ClaimTypes.Name, user.iduser.ToString ())
                    }),
                    Expires = DateTime.UtcNow.AddDays (7),
                    SigningCredentials = new SigningCredentials (new SymmetricSecurityKey (key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken (tokenDescriptor);
                var tokenString = tokenHandler.WriteToken (token);

                // return basic user info (without password) and token to store client side
                return Ok (new User {
                    iduser = user.iduser, roles = user.roles,
                        username = user.username,
                        token = tokenString, avatar = user.avatar
                });
            } catch (System.Exception ex) {
                return BadRequest (new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost ("register")]
        public async Task<IActionResult> Register ([FromBody] touris userDto) {
            // map dto to entity
            using (var db = new OcphDbContext ()) {
                var trans = db.BeginTransaction ();
                try {

                    UserService userService = new UserService ();
                    string password = "MyPassword";
                    User user = new User { username = userDto.email, password = password };

                    if (string.IsNullOrWhiteSpace (password))
                        throw new AppException ("Password is required");
                    var data = db.Users.Where (x => x.username == user.username).FirstOrDefault ();

                    if (data != null)
                        throw new AppException ("Username \"" + user.username + "\" is already taken");

                    byte[] passwordHash, passwordSalt;
                    UserService.CreatePasswordHash (password, out passwordHash, out passwordSalt);

                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;
                    user.iduser = db.Users.InsertAndGetLastID (user);
                    if (user.iduser <= 0)
                        user = null;

                    if (user == null)
                        throw new System.Exception ("Create User Error");

                    userDto.iduser = user.iduser;
                    userDto.idtouris = db.Tourist.InsertAndGetLastID (userDto);
                    if (userDto.idtouris <= 0)
                        throw new System.Exception ();

                    string roleName = "tourist";
                    UserRoleServices userRoleService = new UserRoleServices ();
                    if (!await userRoleService.RoleExsistsAsync (roleName))
                        await userRoleService.CreateRoleAsync (roleName);
                    role roleItem = db.Roles.Find (x => x.name == roleName);

                    if (roleItem == null)
                        throw new System.Exception ($"Role {roleName} Not Found");

                    if (!db.UserRoles.Insert (new userinrole { iduser = user.iduser, idrole = roleItem.idrole }))
                        throw new System.Exception ($"Error Add User To role  {roleName}");

                    var emailService = new EmailServices ();
                    await emailService.SendAsync (new IdentityMessage {
                        Destination = userDto.email,
                            Subject = "Verification Email", Body = $"User Name : {userDto.email} </br> Password:{password}"
                    });

                    trans.Commit ();
                    user.name = userDto.name;
                    user.roles = await userRoleService.GetUserRoleAsync (user);
                    return Ok (user);

                } catch (Exception ex) {
                    trans.Rollback ();
                    // return error message if there was an exception
                    return BadRequest (new { message = ex.Message });
                }
            }
        }

        [HttpGet]
        public IActionResult GetAll () {
            var users = _userService.GetAll ();
            return Ok (users);
        }

        [HttpGet ("{id}")]
        public IActionResult GetById (int id) {
            var user = _userService.GetById (id);
            return Ok (user);
        }

        [HttpGet ("profile")]
        public IActionResult GetTourisProfile () {

            var id = Convert.ToInt32 (User.Identity.Name);
            if (id <= 0) {
                return Unauthorized ();
            }

            var user = _userService.GetTourisProfile (id);
            return Ok (user);
        }

        [HttpPut ("{id}")]
        public IActionResult Put (int id, [FromBody] User userDto) {
            // map dto to entity and set id

            try {
                // save 
                _userService.Update (userDto, userDto.password);
                return Ok ();
            } catch (AppException ex) {
                // return error message if there was an exception
                return BadRequest (new { message = ex.Message });
            }
        }

        [HttpDelete ("{id}")]
        public IActionResult Delete (int id) {
            _userService.Delete (id);
            return Ok ();
        }

        [AllowAnonymous]
        [HttpPost ("resetpassword")]
        public async Task<IActionResult> ResetPassword (string email) {
            try {
                var userData = _userService.GetByEmail (email);
                if (userData != null) {
                    var tokenHandler = new JwtSecurityTokenHandler ();
                    var key = Encoding.ASCII.GetBytes (_appSettings.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor {
                        Subject = new ClaimsIdentity (new Claim[] {
                        new Claim (ClaimTypes.Name, userData.iduser.ToString ())
                        }),
                        Expires = DateTime.UtcNow.AddDays (1),
                        SigningCredentials = new SigningCredentials (new SymmetricSecurityKey (key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken (tokenDescriptor);
                    var tokenString = tokenHandler.WriteToken (token);

                    var callbackUrl = Url.Action (
                        "change", "user",
                        new { userId = userData.iduser, email = email, code = tokenString },
                        Request.Scheme);

                    var emailService = new EmailServices ();
                    await emailService.SendAsync (new IdentityMessage {
                        Destination = userData.username,
                            Body = $"Please Reset your Password by <a href='{callbackUrl}'>clicking here</a>.",
                            Subject = "Reset Password"
                    });

                    if (!string.IsNullOrEmpty (tokenString))
                        return Ok (new { result = "Silahkan Periksa Email Anda Untuk Reset Password" });

                    throw new SystemException ("Coba Ulang Lagi");
                } else {
                    throw new SystemException ("Anda Belum Terdaftar");
                }

            } catch (System.Exception ex) {

                return BadRequest (ex.Message);
            }

        }

        [HttpPost ("changepassword")]
        public IActionResult ChangePasswordAction ([FromBody] ChangePassword data) {
            try {

                if (data != null && !string.IsNullOrEmpty (data.newpassword) &&
                    !string.IsNullOrEmpty (data.confirmpassword) && data.newpassword == data.confirmpassword) {
                    var userData = _userService.GetByEmail (data.email);
                    if (_userService.ChangePassword (data, userData))
                        return Ok (new { result = "Password Berhasil Diubah" });
                }
                throw new SystemException ("Periksa Kembali Data Anda");
            } catch (System.Exception ex) {

                return BadRequest (ex.Message);
            }
        }
    }
}