using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PariwisataWamena.DataAccess;
using PariwisataWamena.Models;

namespace PariwisataWamena.Controllers {
    [Route ("api/[controller]")]
    public class MessageController : Controller {
        private MessageDataAccess context = new MessageDataAccess ();

        [HttpGet]
        public async Task<IActionResult> Get () {
            try {
                var result = await context.Get ();

                return Ok (result);
            } catch (System.Exception ex) {

                return BadRequest (ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post ([FromBody] chat model) {
            try {
                var result = await context.Post (model);
                return Ok (result);

            } catch (System.Exception ex) {

                return BadRequest (ex.Message);
            }
        }

    }
}