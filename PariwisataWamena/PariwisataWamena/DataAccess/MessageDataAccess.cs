using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using PariwisataWamena.Models;
using System.Collections.Generic;
using PariwisataWamena.Services;
using System;
using System.Linq;

namespace PariwisataWamena.DataAccess
{
    public class MessageDataAccess: IDataAccess<chat> {
        private OcphDbContext context = new OcphDbContext ();
        public Task<bool> Delete (int id) {
            throw new System.NotImplementedException ();
        }

        public Task<IEnumerable<chat>> Get () {
            try {
                return Task.FromResult<IEnumerable<chat>> (context.Chats.Select ());
            } catch (System.Exception) {

                throw;
            }
        }

        public Task<agent> Get (int id) {
            try {
                return Task.FromResult (context.Agent.Find (x => x.idagent == id));
            } catch (System.Exception) {

                throw;
            }
        }

        public async Task<chat> Post (chat t) {
            using (var db = new OcphDbContext ()) {
                try {
                   t.id = context.Chats.InsertAndGetLastID(t);
                   if(t.id>0)
                   return await Task.FromResult(t);
                   throw new SystemException("Not Saved");
                } catch (System.Exception ex) {
                    throw new System.Exception(ex.Message);
                }
            }
        }

        public Task<bool> Put(int id, chat t)
        {
            throw new NotImplementedException();
        }

        Task<chat> IDataAccess<chat>.Get(int id)
        {
            throw new NotImplementedException();
        }
    }
}