using System.Runtime.InteropServices;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PariwisataWamena.Models;
using PariwisataWamena.Services;
using System;

namespace PariwisataWamena.DataAccess
{
    public class LayananDataAccess
    {
        private int agentId;

        public LayananDataAccess(int agentid)
        {
            this.agentId = agentid;
        }

        public LayananDataAccess()
        {
        }

        public Task<layanan> CretaLayanan(layanan data)
        {
            using (var db = new OcphDbContext())
            {
                try
                {
                    data.idservice = db.Layanan.InsertAndGetLastID(data);
                    if (data.idservice <= 0)
                        throw new System.Exception("Data Not Saved");
                    else
                        return Task.FromResult(data);

                }
                catch (System.Exception ex)
                {

                    throw new System.Exception(ex.Message);
                }
            }
        }

        internal Task<List<layanan>> GetLayanan()
        {
            using (var db = new OcphDbContext())
            {
                var result = from a in  db.Layanan.Where(O => O.idagent == this.agentId)
                join b in db.Article.Select() on a.idarticle equals b.idarticle
                join c in db.Agent.Select() on a.idagent equals c.idagent
                select new layanan{ agent=c, active= a.active,content=a.content,idagent=a.idagent,idarticle=a.idarticle,
                idservice= a.idservice,name= a.name,price=a.price, article=b };
                return Task.FromResult(result.ToList());
            }
        }

        internal Task<List<layanan>> GetLayananAll()
        {
            using (var db = new OcphDbContext())
            {
                var result = from a in db.Layanan.Select()
                             join b in db.Agent.Select() on a.idagent equals b.idagent
                               join c in db.Article.Select() on a.idarticle equals c.idarticle
                             select new layanan
                             {
                                 active = a.active,
                                 content = a.content,
                                 idagent = a.idagent,
                                 idservice = a.idservice,
                                 name = a.name,
                                 price = a.price, 
                                 idarticle=a.idarticle,
                                 agent = b,article = c
                             };
                return Task.FromResult(result.ToList());
            }
        }


        internal Task<List<transaction>> GetTransaction(int serviceId)
        {
            using (var db = new OcphDbContext())
            {
                var result = from a in db.Layanan.Where(o => o.idagent == agentId)
                             join b in db.Transactions.Select() on a.idservice equals b.idservice
                             select b;
                return Task.FromResult(result.ToList());
            }
        }

        public Task<bool> UpdateLayanan(layanan data)
        {
            using (var db = new OcphDbContext())
            {
                try
                {
                    var saved = db.Layanan.Update(x => new { x.content, x.price, x.idarticle }, data, x => x.idservice == data.idservice);
                    if (!saved)
                        throw new System.Exception("Data Not Saved");
                    else
                        return Task.FromResult(true);

                }
                catch (System.Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
        }

        public Task<bool> ConfirmTransaction(transaction modal)
        {
            modal.status="success";
            using (var db = new OcphDbContext())
            {
                var updated = db.Transactions.Update(O => new { O.count, O.end, O.status, O.start }, modal, o => o.idtransaction == modal.idtransaction);

                return Task.FromResult(updated);
            }
        }

        internal Task<transaction> CreateNewTransaction(transaction model)
        {
            using (var db = new OcphDbContext())
            {
                try
                {
                    model.idtransaction = db.Transactions.InsertAndGetLastID(model);
                    if (model.idtransaction > 0)
                        return Task.FromResult(model);
                    else
                        throw new SystemException("Not Saved");
                }
                catch (System.Exception ex)
                {
                    throw new SystemException(ex.Message);
                }
            }
        }

        internal Task<List<transaction>> GetTransactionByAgentId(int id)
        {
            using (var db = new OcphDbContext())
            {
                var result = (from a in db.Transactions.Select()
                              join e in db.Tourist.Select() on a.idtouris equals e.idtouris
                              join b in db.Layanan.Where(x => x.idagent == id) on a.idservice equals b.idservice
                              join c in db.Agent.Select() on b.idagent equals c.idagent
                              select new transaction
                              {
                                  count = a.count,
                                  end = a.end,
                                  idservice = a.idservice,
                                  idtouris = a.idtouris,
                                  idtransaction = a.idtransaction,
                                  start = a.start,
                                  tourist = e,
                                  agent = c,
                                  layanan = b, status=a.status
                              }).ToList();

                foreach (var item in result)
                {
                    item.payment = db.Payments.Where(x => x.idtransaction == item.idtransaction).FirstOrDefault();
                }

                return Task.FromResult(result.ToList());
            }
        }

        internal Task<List<transaction>> GetTransactionByTouristId(int id)
        {
            using (var db = new OcphDbContext())
            {
                var result = (from a in db.Transactions.Where(x => x.idtouris == id)
                              join b in db.Layanan.Select() on a.idservice equals b.idservice
                              join c in db.Agent.Select() on b.idagent equals c.idagent
                              select new transaction
                              {
                                  count = a.count,
                                  end = a.end, status=a.status,
                                  idservice = a.idservice,
                                  idtouris = a.idtouris,
                                  idtransaction = a.idtransaction,
                                  start = a.start,
                                  agent = c,
                                  layanan = b
                              }).ToList();
                foreach (var item in result)
                {
                    item.payment = db.Payments.Where(x => x.idtransaction == item.idtransaction).FirstOrDefault();
                }
                return Task.FromResult(result.ToList());
            }
        }

        internal Task<payment> CreatePayment(payment model)
        {
           using (var db = new OcphDbContext())
            {
                var trans=db.BeginTransaction();
                try
                {
                    model.idpayment = db.Payments.InsertAndGetLastID(model);
                    if(model.idpayment<=0)
                        throw new SystemException("Data Not Saved");
                    
                    if(!db.Transactions.Update(x=>new{x.status}, new transaction{status="Menunggu Konfirmasi"},x=>x.idtransaction==model.idtransaction))
                        throw new SystemException("Data Not Saved");

                    trans.Commit();

                    return Task.FromResult(model);

                }
                catch (System.Exception ex)
                {
                    
                    throw new SystemException(ex.Message);
                }
            }
        }

        public agent GetAgentEmailByTransactionId(int id)
        {

            using (var db = new OcphDbContext())
            {
                return (from a in db.Transactions.Where(O=>O.idtransaction==id)
                join b in db.Layanan.Select() on a.idservice equals b.idservice
                join c in db.Agent.Select() on b.idagent equals c.idagent
                select c).FirstOrDefault();

            }

        }

          public touris GetTouristEmailByTransactionId(int id)
        {

            using (var db = new OcphDbContext())
            {
                return (from a in db.Transactions.Where(O=>O.idtransaction==id)
                join c in db.Tourist.Select() on a.idtouris equals c.idtouris
                select c).FirstOrDefault();

            }

        }
    }
}