using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using PariwisataWamena.Models;
using PariwisataWamena.Services;

namespace PariwisataWamena.DataAccess {
    public class StatisticDataAccess {

        public Task<object> Get () {
            try {

                using (var db = new OcphDbContext ()) {

                    var tgl = DateTime.Now;
                    var cmd = db.CreateCommand ();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"Select COUNT(*) from statistic where date(tanggal)= date('{tgl.Year}-{tgl.Month}-{tgl.Day}')";
                    var today = Convert.ToInt32 (cmd.ExecuteScalar ());

                    cmd.CommandText = $"Select COUNT(*) from statistic where month(tanggal)= {tgl.Month}";
                    var month = Convert.ToInt32 (cmd.ExecuteScalar ());

                    cmd.CommandText = $"Select COUNT(*) from statistic where year(tanggal)= {tgl.Year}";
                    var year = Convert.ToInt32 (cmd.ExecuteScalar ());

                    object result = new { today = today, month = month, year = year, online = 0 };
                    return Task.FromResult (result);
                }

            } catch (System.Exception ex) {

                throw new SystemException (ex.Message);
            }
        }

        public Task<bool> Post (statistic item) {
            try {
                using (var db = new OcphDbContext ()) {
                    var tgl = DateTime.Now;
                    var cmd = db.CreateCommand ();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"Select COUNT(*) from statistic where date(tanggal)=date('{tgl.Year}-{tgl.Month}-{tgl.Day}') and ip='{item.IpAddress}'";
                    var today = Convert.ToInt32 (cmd.ExecuteScalar ());
                    if (today <= 0)
                        return Task.FromResult (db.Statistics.Insert (item));
                    throw new SystemException ("User IP Is Register Today");
                }

            } catch (System.Exception ex) {

                throw new SystemException (ex.Message);
            }
        }

    }
}