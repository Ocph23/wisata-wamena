import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['../authentification.component.scss', './reset.component.css']
})

export class ResetComponent implements OnInit {
  loginForm: FormGroup;
  @ViewChild('swalMessage') swal: SwalComponent;
  constructor(private auth: AuthService,
    private alertService: AlertService,
    fb: FormBuilder) {
    this.loginForm = fb.group({
      'email': [null, Validators.required],
    });
  }


  ngOnInit() {
   this.alertService.setSwal(this.swal);
  }

  Reset(item: any) {
      this.auth.reset(item.email).subscribe(x => {
        this.alertService.success('Success', x.result);
       },err => {
        this.alertService.error('Error', err.message);
       });
  }
}
