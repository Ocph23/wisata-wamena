import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from '../auth.service';
import { IChangePassword } from 'src/app/models/models.component';
import { ActivatedRoute } from '@angular/router';
import { error } from 'util';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['../authentification.component.scss', './change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changeForm: FormGroup;
  @ViewChild('swalMessage') swal: SwalComponent;
  email: any;
  constructor(private auth: AuthService,
    private alertService: AlertService, private router: ActivatedRoute,
    fb: FormBuilder) {
    this.changeForm = fb.group({
      'newPassword': [null, Validators.required],
      'confirmPassword': [null, Validators.required],
    });
  }

  token: string;

  ngOnInit() {
    this.alertService.setSwal(this.swal);
    this.router.queryParams.subscribe(params => {
      const a = params['userId'];
      this.token = params['code'];
      this.email = params['email'];
    });
    if (!this.email) {
      this.alertService.error('Error', 'Anda Tidak Dapat Mengubah Password');
    }
  }

   ChangePassword(item: IChangePassword) {
    item.email = this.email;
     this.auth.change(item, this.token).subscribe(x => {
      this.alertService.success('Success', x.result);
     }, err => {
      this.alertService.error('Error', err.message);
     });
  }
}
