import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../authentification.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('swalMessage') private swal: SwalComponent;
   message = '';
  private _success = new Subject<string>();
   staticAlertClosed = true;
  genders: string[] = ['', 'Pria', 'Wanita'];
  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private alertService: AlertService) {
    this.registerForm = fb.group({
      'nik': [null],
      'email': [null, Validators.required],
      'name': [null, Validators.required],
      'gender': [null, Validators.required],
      'address': [null, Validators.required],
      'telepon': [null, Validators.required],
    });





  }

  ngOnInit() {
    this.alertService.setSwal(this.swal);
    this._success.subscribe((param) => {
      this.staticAlertClosed = false;
      this.message = param; });
    this._success.pipe(
      debounceTime(7000)
    ).subscribe(() => {
      this.message = null;
      this.staticAlertClosed = true;
    });
  }

  register(item) {
    this.auth.register(item)
    .subscribe(
      result => {
        this.auth.storage.addObject('user', result);
        this.alertService.success('success', `Register Success, Check Your Email And Confirm Email !`);
        this.router.navigate(['/user/login']);
      },
      error => {
        this.alertService.error('error', error.error.message);
      }
    );
  }

}
