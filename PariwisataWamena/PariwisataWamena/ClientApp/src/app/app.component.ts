import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SignalRService } from './services/signal-r.service';
import { AuthService } from './authentification/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'app';
  constructor(private http: HttpClient, private signalr: SignalRService,
    private auth: AuthService, @Inject('BASE_URL') private baseUrl: string) { }

  ngOnInit() {
    this.http.get('https://jsonip.com/').subscribe(data => {
      this.saveIP(data['ip']);
    });

  }

  async saveIP(address: string) {
    const result = await this.http.post<string>(this.baseUrl + 'api/Statistic', '"' + address + '"', this.auth.getHttpHeader()).toPromise();
    return result;
  }
}
