import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { IArticle, PanelArticle, ArticleType, IModalResult } from 'src/app/models/models.component';
import { ArticleService } from 'src/app/services/article.service';
import { AlertService } from 'src/app/services/alert.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminAddArticleComponent } from '../admin-add-article/admin-add-article.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-destinasi',
  templateUrl: './admin-destinasi.component.html',
  styleUrls: ['../admin.component.scss', './admin-destinasi.component.scss']
})


export class AdminDestinasiComponent implements OnInit {
  @ViewChild('swalMessage') swal: SwalComponent;
  // @ViewChild(AdminAddArticleComponent) addArticle: AdminAddArticleComponent;
  Data: PanelArticle = { selected: null, datas: [] };
  form: FormGroup;
  constructor(
    private router: Router,
    private articleService: ArticleService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) { }
  DataSelected: any;

  ngOnInit() {

    this.alertService.setSwal(this.swal);
    this.articleService.getData(ArticleType.Destinasi).then(x => {
      this.Data = x;
    });
  }

    editItem(data: IArticle) {
      const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
      ref.componentInstance.form = this.articleService.createEditForm(data, 'Destinasi');
      ref.result.then(x => {
        const result = x as IModalResult;
        data.thumb = result.data.thumb;
        this.alertService.success(null, 'success');
      });
    }



    addNewItem() {
      const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
      ref.componentInstance.form = this.articleService.createNewForm('Destinasi');
      ref.result.then(x => {
        const result = x as IModalResult;
        this.alertService.success(null, 'success');
        this.Data.datas.push(result.data);
      });
    }


    async savePublish(item: IArticle) {
      const data: any = Object.assign({}, item);
      const result = await this.articleService.publish(data);
      if (result) {
        item.status = 'publish';
    }
    }

    async saveUnpublish(item: IArticle) {
      const data: any = Object.assign({}, item);
    const result = await this.articleService.unPublish(data);
      if (result) {
          item.status = 'unpublish';
      }
    }
}
