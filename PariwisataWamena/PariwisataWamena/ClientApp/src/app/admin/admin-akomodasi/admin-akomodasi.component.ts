import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IArticle, PanelArticle, ArticleType, IModalResult } from 'src/app/models/models.component';
import { ArticleService } from 'src/app/services/article.service';
import { FormGroup } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AdminAddArticleComponent } from '../admin-add-article/admin-add-article.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin-akomodasi',
  templateUrl: './admin-akomodasi.component.html',
  styleUrls: ['../admin.component.scss', './admin-akomodasi.component.scss']
})


export class AdminAkomodasiComponent implements OnInit {
  @ViewChild('swalMessage') swal: SwalComponent;
 // @ViewChild(AdminAddArticleComponent) addArticle: AdminAddArticleComponent;
   Akomodasi: PanelArticle = { selected: null, datas: [] };
    form: FormGroup;
  constructor(
    private router: Router,
    private articleService: ArticleService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) { }
  DataSelected: any;

  ngOnInit() {

    this.alertService.setSwal(this.swal);
    this.articleService.getData(ArticleType.Akomodasi).then(x => {
      this.Akomodasi = x;
    });
  }


  goTo () {

    const data = {'Type': 'Akomodasi', 'Title': '', 'Content': 'Siapa'};
    this.router.navigate(['/admin/article',"Akomodasi"]);
  }

  editItem(data: IArticle) {
    const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
    ref.componentInstance.form = this.articleService.createEditForm(data, 'Akomodasi');
    ref.result.then(x => {
      const result = x as IModalResult;
      data.thumb = result.data.thumb;
      this.alertService.success(null, 'success');
    });
  }



  addNewItem() {
    const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
    ref.componentInstance.form = this.articleService.createNewForm('Akomodasi');
    ref.result.then(x => {
      const result = x as IModalResult;
      this.alertService.success(null, 'success');
      this.Akomodasi.datas.push(result.data);
    });
  }

  
  async savePublish(item: IArticle) {
    const data: any = Object.assign({}, item);
    const result = await this.articleService.publish(data);
    if (result) {
      item.status = 'publish';
  }
  }

  async saveUnpublish(item: IArticle) {
    const data: any = Object.assign({}, item);
   const result = await this.articleService.unPublish(data);
    if (result) {
        item.status = 'unpublish';
    }
  }

}
