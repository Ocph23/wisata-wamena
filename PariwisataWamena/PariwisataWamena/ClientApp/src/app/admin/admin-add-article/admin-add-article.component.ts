import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IArticle } from 'src/app/models/models.component';
import { FormGroup } from '@angular/forms';
import { ArticleService } from 'src/app/services/article.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'adminaddarticle',
  templateUrl: './admin-add-article.component.html',
  styleUrls: ['../admin.component.scss', './admin-add-article.component.scss']
})

export class AdminAddArticleComponent implements OnInit {



  @Input() form: FormGroup;
  tinymceInit: any;

  title: string;
  header: any;
  constructor(private router: ActivatedRoute,
    private articleService: ArticleService,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal
  ) {
    this.router.paramMap.subscribe(params => {
      this.header = params.get("title")
    })
    this.tinymceInit = {
      height: 500,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste imagetools wordcount'
      ],
      // tslint:disable-next-line:max-line-length
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
      ],

      images_upload_handler: function (blobInfo, success, failure) {
        let xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'api/Image/upload');

        xhr.onload = function() {
          let json;

          if (xhr.status < 200 || xhr.status >= 300) {
          failure('HTTP Error: ' + xhr.status);
          return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.imageUrl !== 'string') {
          failure('Invalid JSON: ' + xhr.responseText);
          return;
          }

          success(json.imageUrl);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
        }

    };

  }

  ngOnInit() {
    // this.form = this.articleService.createNewForm('Akomodasi');
  }

  saveDraft(data: IArticle) {
    this.articleService.saveDraft(data).subscribe(x => {
     
      const imgs = this.articleService.getImagesFromContent(data.draft);
      if (imgs != null && imgs.length > 0) {
        data.thumb = imgs[0];
      } else {
        data.thumb = '/uploads/noimage.jpg';
      }

      const result = { success: true, data: data };
      this.activeModal.close(result);
    });
  }

  savePublish(data: IArticle) {
    data.status = 'publish';
    this.articleService.savePublish(data).subscribe(x => {
      const imgs = this.articleService.getImagesFromContent(data.draft);
      if (imgs != null && imgs.length > 0) {
        data.thumb = imgs[0];
      } else {
        data.thumb = '/uploads/noimage.jpg';
      }

      const result = { success: true, data: data };

      this.activeModal.close(result);
    });
  }


  addItem(content) {
    this.modalService.open(content, {
      size: 'lg', backdrop: 'static'
    });
  }

  public editItem(data: IArticle): void {
    this.form = this.articleService.createEditForm(data, data.type);
  }

}

