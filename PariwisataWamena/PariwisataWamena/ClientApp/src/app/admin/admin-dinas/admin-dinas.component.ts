import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { IArticle, PanelArticle, ArticleType } from 'src/app/models/models.component';
import { ArticleService } from 'src/app/services/article.service';
import { AuthService } from 'src/app/authentification/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-admin-dinas',
  templateUrl: './admin-dinas.component.html',
  styleUrls: ['../admin.component.scss', './admin-dinas.component.scss']
})
export class AdminDinasComponent implements OnInit {
 
  @ViewChild('swalMessage') swal: SwalComponent;
  Data: PanelArticle;

 public Selected: IArticle = {title: 'Profile', draft: 'type here'} as IArticle;
  tinymceInit: {
  height: number; plugins: string[];
    // tslint:disable-next-line:max-line-length
    toolbar: string; content_css: string[]; images_upload_handler: (blobInfo: any, success: any, failure: any) => void;
  };

  constructor(
    private alertService: AlertService,
    private auth: AuthService,
    private dinasService: ArticleService,
    @Inject('BASE_URL') private baseUrl: string
  ) {
    this.tinymceInit = {
      height: 500,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste imagetools wordcount'
      ],
      // tslint:disable-next-line:max-line-length
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
      ],

      images_upload_handler: function (blobInfo, success, failure) {
        let xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'api/Image/upload');

        xhr.onload = function() {
          let json;

          if (xhr.status < 200 || xhr.status >= 300) {
          failure('HTTP Error: ' + xhr.status);
          return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.imageUrl !== 'string') {
          failure('Invalid JSON: ' + xhr.responseText);
          return;
          }

          success(json.imageUrl);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
        }

    };

    dinasService.getData(ArticleType.Dinas).then(x => {
      this.Data = x;
     this.getContent('profile');
      });


  }

  public getContent(title: string) {
    if (this.Data == null) {
       this.Selected = {title: title.toUpperCase(), draft: 'type ' + title + ' here ...'} as IArticle;
    } else {
        const findx = this.Data.datas.find(x => x.title.toLowerCase() === title.toLowerCase());
        if (findx != null) {
          this.Selected = findx;
        } else {
          this.Selected = {title: title.toUpperCase(), draft: 'type ' + title + ' here ...'} as IArticle;
        }
       }

  }

  public save(data: IArticle, status: string) {
    data.status = status;
    if (status === 'publish') {
      data.content = data.draft;
    }
    data.iduser = 1;
    data.type = ArticleType.Dinas;
    this.dinasService.SaveArticle(data).subscribe(x => {
      this.alertService.success(null, 'success');

    }, error => {console.log(error); });
  }

  ngOnInit() {
    this.alertService.setSwal(this.swal);
  }

}
