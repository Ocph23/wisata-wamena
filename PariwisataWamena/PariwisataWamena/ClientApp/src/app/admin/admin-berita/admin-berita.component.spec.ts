import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBeritaComponent } from './admin-berita.component';

describe('AdminBeritaComponent', () => {
  let component: AdminBeritaComponent;
  let fixture: ComponentFixture<AdminBeritaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBeritaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBeritaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
