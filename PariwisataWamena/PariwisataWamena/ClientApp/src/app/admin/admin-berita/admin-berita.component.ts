import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup } from '@angular/forms';
import { PanelArticle, ArticleType, IArticle, IModalResult } from 'src/app/models/models.component';
import { AlertService } from 'src/app/services/alert.service';
import { AdminAddArticleComponent } from '../admin-add-article/admin-add-article.component';

@Component({
  selector: 'app-admin-berita',
  templateUrl: './admin-berita.component.html',
  styleUrls: ['./admin-berita.component.scss', '../admin.component.scss']
})
export class AdminBeritaComponent implements OnInit {

  @ViewChild('swalMessage') swal: SwalComponent;
  // @ViewChild(AdminAddArticleComponent) addArticle: AdminAddArticleComponent;
   Berita: PanelArticle = { selected: null, datas: [] };
  form: FormGroup;
  constructor(
    private router: Router,
    private articleService: ArticleService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) { }
  DataSelected: any;

  ngOnInit() {

    this.alertService.setSwal(this.swal);
    this.articleService.getData(ArticleType.Berita).then(x => {
      this.Berita = x;
    });
  }

  editItem(data: IArticle) {
    const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
    ref.componentInstance.form = this.articleService.createEditForm(data, data.type);
    ref.result.then(x => {
      const result = x as IModalResult;
      data.thumb = result.data.thumb;
      this.alertService.success(null, 'success');
    });
  }



  addNewItem() {
    const ref = this.modalService.open(AdminAddArticleComponent, {size: 'lg', backdrop: 'static' });
    ref.componentInstance.form = this.articleService.createNewForm('Berita');
    ref.result.then(x => {
      const result = x as IModalResult;
      this.alertService.success(null, 'success');
      this.Berita.datas.push(result.data);
    });
  }

  
  async savePublish(item: IArticle) {
    const data: any = Object.assign({}, item);
    const result = await this.articleService.publish(data);
    if (result) {
      item.status = 'publish';
  }
  }

  async saveUnpublish(item: IArticle) {
    const data: any = Object.assign({}, item);
   const result = await this.articleService.unPublish(data);
    if (result) {
        item.status = 'unpublish';
    }
  }

}
