import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { PanelArticle, ArticleType, IArticle, IModalResult } from 'src/app/models/models.component';
import { FormGroup } from '@angular/forms';
import { ArticleService } from 'src/app/services/article.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/services/alert.service';
import { AdminAddArticleComponent } from '../admin-add-article/admin-add-article.component';

@Component({
  selector: 'app-admin-kuliner',
  templateUrl: './admin-kuliner.component.html',
  styleUrls: ['../admin.component.scss', './admin-kuliner.component.scss']
})
export class AdminKulinerComponent implements OnInit {
  @ViewChild('swalMessage') swal: SwalComponent;
  // @ViewChild(AdminAddArticleComponent) addArticle: AdminAddArticleComponent;
  Akomodasi: PanelArticle = { selected: null, datas: [] };
  form: FormGroup;
  constructor(
    private router: Router,
    private articleService: ArticleService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) { }
  DataSelected: any;

  ngOnInit() {

    this.alertService.setSwal(this.swal);
    this.articleService.getData(ArticleType.Kuliner).then(x => {
      this.Akomodasi = x;
    });
  }

  editItem(data: IArticle) {
    const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static' });
    ref.componentInstance.form = this.articleService.createEditForm(data, 'Kuliner');
    ref.result.then(x => {
      const result = x as IModalResult;
      data.thumb = result.data.thumb;
      this.alertService.success(null, 'success');
    });
  }



  addNewItem() {
    const ref = this.modalService.open(AdminAddArticleComponent, { size: 'lg', backdrop: 'static'});
    ref.componentInstance.form = this.articleService.createNewForm('Kuliner');
    ref.result.then(x => {
      const result = x as IModalResult;
      this.alertService.success(null, 'success');
      this.Akomodasi.datas.push(result.data);
    });
  }


  

  async savePublish(item: IArticle) {
    const data: any = Object.assign({}, item);
    const result = await this.articleService.publish(data);
    if (result) {
      item.status = 'publish';
  }
  }

  async saveUnpublish(item: IArticle) {
    const data: any = Object.assign({}, item);
   const result = await this.articleService.unPublish(data);
    if (result) {
        item.status = 'unpublish';
    }
  }

}
