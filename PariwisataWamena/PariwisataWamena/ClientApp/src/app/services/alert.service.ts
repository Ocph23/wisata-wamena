import { Injectable } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { SweetAlertType } from 'sweetalert2';
import { faTasks } from '@fortawesome/free-solid-svg-icons';
import { delay } from 'q';

@Injectable({
  providedIn: 'root'
})


export class AlertService {
  private title = 'info';
  private  message: string;
  private  type: SweetAlertType;
  private swal: SwalComponent;
  constructor() { }

  async setSwal(swal: SwalComponent) {
    this.swal = swal;
  }


  async success(title, message) {
    await delay(50);
    (!title ? this.setTitle(Kind.success) : this.title = title);
    this.swal.type = 'success';
    this.message = message;
     this.show();
  }


  async info(title, message) {
    await delay(50);
    (!title ? this.setTitle(Kind.info) : this.title = title);
    this.swal.type = 'info';
    this.message = message;
    this.show();
  }


  async warning(title, message) {
    await delay(50);
    (!title ? this.setTitle(Kind.warning) : this.title = title);
    this.swal.type = 'warning';
    this.message = message;
    this.show();
  }



 async error(title: string, message: string) {
    await delay(50);
    (!title ? this.setTitle(Kind.error) : this.title = title);
    this.swal.type = 'error';
    this.message = message;
    this.show();
  }




  async question(title, message) {
    await delay(50);
    (!title ? this.setTitle(Kind.question) : this.title = title);
    this.swal.type = 'question';
    this.message = message;
    this.show();
  }


  private show() {
    this.swal.text = this.message;
    this.swal.title = this.title;
    this.swal.show();
  }


  private setTitle(type: Kind) {
    switch (type) {
      case Kind.success:
        this.title = 'Success';
        break;
      case Kind.error:
        this.title = 'Error';
        break;
      case Kind.warning:
        this.title = 'Warning';
        break;
      case Kind.question:
        this.title = 'Question';
        break;
      default:
        this.title = 'Info';
    }
  }

}


enum Kind {
  success,
  warning,
  error,
  info,
  question
}


