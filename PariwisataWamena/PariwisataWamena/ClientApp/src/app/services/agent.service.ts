import { Injectable, Inject } from '@angular/core';
import { IAgent, ILayanan, IPayment, ITransaction } from '../models/models.component';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../authentification/auth.service';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})

export class AgentService {

  private instance = false;
  public source: IAgent[];
  sourceLayanan: ILayanan[];
  Selected: IAgent;
  LayananSelected: ILayanan;
  Transactions: ITransaction[];
  instanceLayanan: any;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private alert: AlertService,
    @Inject('BASE_URL') private baseUrl: string,
    private router: Router,
    private auth: AuthService
  ) {
    this.get();
  }

  async createPayment(item: IPayment): Promise<IPayment> {
    const result = await this.http.post<IPayment>(this.baseUrl + 'api/Service/payment', item, this.auth.getHttpHeader()).toPromise();
    return result;
  }


  async confirmPayment(item: ITransaction) {
   try {
      const result = await this.http.post<ITransaction>(this.baseUrl + 'api/Service/confirmtransaction',
      item, this.auth.getHttpHeader()).toPromise();
      return result;
   } catch (error) {
      this.alert.error('Error', error.message);
   }
  }



  saveNewLayanan(model: ILayanan) {
    try {
      if (model.idservice !== undefined && model.idservice > 0) {
        return this.http.put<ILayanan>(
          this.baseUrl + 'api/Service?id=' + model.idservice,
          model,
          this.auth.getHttpHeader()
        );
      } else {
        return this.http.post<ILayanan>(
          this.baseUrl + 'api/service',
          model,
          this.auth.getHttpHeader()
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  public getLayanan() {
    return new Promise<ILayanan[]>((p, r) => {
      try {
        if (!this.instanceLayanan) {
          this.http
            .get<ILayanan[]>(
              this.baseUrl + 'api/service/all',
              this.auth.getHttpHeader()
            )
            .subscribe(
              result => {
                this.instanceLayanan = true;
                result.forEach(x => {
                  if (x.content !== null) {
                    x.desc = x.content
                      .replace(/<\/?[^>]+(>|$)/g, '')
                      .substring(0, 200);
                    const html = document.createElement('html');
                    html.innerHTML = x.content;
                    const imgs = html.getElementsByTagName('img')[0];
                    if (imgs != null) {
                      x.thumb = imgs.src;
                    } else {
                      x.thumb = '/uploads/noimage.jpg';
                    }
                  } else {
                    x.thumb = '/uploads/noimage.jpg';
                  }
                });
                this.sourceLayanan = result;
                p(result);
              },
              error => {
                throw new Error(error);
              }
            );
        } else {
          p(this.sourceLayanan);
        }
      } catch (error) {
        r(error);
      }
    });
  }

  public get() {
    return new Promise<IAgent[]>((p, r) => {
      try {
        if (!this.instance) {
          this.http
            .get<IAgent[]>(this.baseUrl + 'api/agent', this.auth.getHttpHeader())
            .subscribe(
              result => {
                this.instance = true;
                result.forEach(x => {
                  if (x.profile != null) {
                    x.desc = x.profile
                      .replace(/<\/?[^>]+(>|$)/g, '')
                      .substring(0, 200);
                    const html = document.createElement('html');
                    html.innerHTML = x.profile;
                    const imgs = html.getElementsByTagName('img')[0];
                    if (imgs != null) {
                      x.thumb = imgs.src;
                    } else {
                      x.thumb = '/uploads/noimage.jpg';
                    }
                  } else { x.thumb = '/uploads/noimage.jpg'; }
                });
                this.source = result;
                p(result);
              },
              error => {
                throw new Error(error);
              }
            );
        } else {
          p(this.source);
        }
      } catch (error) {
        r(error);
      }
    });
  }

  async getTransaction(agentId: number) {
    if (!this.Transactions) {
      const res = await this.http.get<ITransaction[]>('api/service/transaction/' + agentId, this.auth.getHttpHeader()).toPromise();
      if (res) {
      this.Transactions = res;
      }
    }
    return this.Transactions;

  }

  createNewLayananForm() {
    return this.fb.group({
      active: [true],
      idagent: [0, Validators.required],
      content: ['', Validators.required],
      idservice: [0, Validators.required],
      idarticle:[0,Validators.required],
      name: ['', Validators.required],
      article: [null],
      price: [0, Validators.required]
    });
  }

  createEditLayananForm(data: ILayanan) {
    return this.fb.group({
      active: [data.active],
      idagent: [data.idagent, Validators.required],
      content: [data.content, Validators.required],
      idarticle:[data.idarticle,Validators.required],
      idservice: [data.idservice, Validators.required],
      name: [data.name, Validators.required],
      article:[data.article],
      price: [data.price, Validators.required]
    });
  }

}
