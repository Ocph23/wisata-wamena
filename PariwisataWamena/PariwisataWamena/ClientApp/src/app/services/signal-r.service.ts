import { Injectable, Inject } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { IChat, IStatistic } from '../models/models.component';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../authentification/auth.service';
import { async } from 'q';
import { post } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  public datas: IChat[] = [];
  public statistic: IStatistic = {online: 0, today: 0, month: 0, year: 0} as IStatistic;

  /**
   *
   */
  constructor(@Inject('BASE_URL') private baseUrl: string, private http: HttpClient, private auth: AuthService) {
    this.LoadData();
  }
  public hubConnection: signalR.HubConnection;
  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('/chatHub')
      .build();

    this.hubConnection.on('ReceiveMessage', (data, message) => {
      this.datas.push({ nama: data, pesan: message, tanggal: new Date() });
      console.log(data);
    });

    this.hubConnection.on('OnConnect', (data) => {
      this.statistic.online = data;
     });

     this.hubConnection.on('OnDisconnect', (data) => {
       this.statistic.online = data;
      });

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }


  async LoadData() {
    try {
      const result = await this.http.get<IChat[]>(this.baseUrl + 'api/Message',
        this.auth.getHttpHeader()).toPromise();
      this.datas = result;
     await this.LoadStatistic();
     this.startConnection();
    } catch (error) {
      //
    }
  }

  async LoadStatistic() {
    const result = await this.http.get<IStatistic>(this.baseUrl + 'api/Statistic',
    this.auth.getHttpHeader()).toPromise();
    this.statistic.today = result.today;
    this.statistic.month = result.month;
    this.statistic.year = result.year;
  }

  async post(item: IChat) {
    if (this.auth.hasLogin() && this.auth.IsInRole('Admin')) {
      item.nama = 'Admin';
    } else if (this.auth.hasLogin()) {
      item.nama = this.auth.userName();
    } else {
      item.nama = 'Guest';
    }
    item.tanggal = new Date();

    try {
      const result = await this.http.post<IChat>(this.baseUrl + 'api/Message',
        item, this.auth.getHttpHeader()).toPromise();
      if (result) {
        this.hubConnection.invoke('SendMessage', item.nama, item.pesan);
        return result;
      }
    } catch (error) {
      //
    }

  }





}
