import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "src/app/authentification/auth.service";
import {
  ILayanan,
  IModalResult,
  ArticleType,
  IArticle
} from "src/app/models/models.component";
import { AlertService } from "src/app/services/alert.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup } from "@angular/forms";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import swal from "sweetalert2";
import { AgentService } from "src/app/services/agent.service";
import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { ArticleService } from "src/app/services/article.service";

@Component({
  selector: "app-agent-layanan",
  templateUrl: "./layanan.component.html",
  styleUrls: ["./layanan.component.scss", "../agent.component.scss"]
})
export class LayananComponent implements OnInit {
  @ViewChild("messageSwal") private messageSwal: SwalComponent;
  tinymceInit = {
    height: 500,
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table paste imagetools wordcount"
    ],
    // tslint:disable-next-line:max-line-length
    toolbar:
      "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    content_css: [
      "//fonts.googleapis.com/css?family=Lato:300,300i,400,400i",
      "//www.tiny.cloud/css/codepen.min.css"
    ],

    images_upload_handler: function(blobInfo, success, failure) {
      let xhr, formData;

      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open("POST", "api/Image/upload");

      xhr.onload = function() {
        let json;

        if (xhr.status < 200 || xhr.status >= 300) {
          failure("HTTP Error: " + xhr.status);
          return;
        }

        json = JSON.parse(xhr.responseText);

        if (!json || typeof json.imageUrl !== "string") {
          failure("Invalid JSON: " + xhr.responseText);
          return;
        }

        success(json.imageUrl);
      };

      formData = new FormData();
      formData.append("file", blobInfo.blob(), blobInfo.filename());

      xhr.send(formData);
    }
  };

  layanans: ILayanan[];
  closeResult: string;
  form: FormGroup;
  Destinasi: IArticle[];
  constructor(
    private authService: AuthService,
    public modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private articleService: ArticleService,
    private agentService: AgentService,
    private alertService: AlertService
  ) {
    agentService.getLayanan().then(x => {
      this.layanans = x;
    });
  }
  ngOnInit() {
    this.alertService.setSwal(this.messageSwal);

    this.articleService.getData(ArticleType.Destinasi).then(x => {
      this.Destinasi = x.datas;
    });
  }

  compare(val1, val2) {
    if (val2 === null) 
      return false;
    else 
      return val1.idarticle === val2.idarticle;
  }

  SetArticleId(item) {
    item.idarticle = item.article.idarticle;
  }

  addNewItem(content) {
    this.form = this.agentService.createNewLayananForm();
    this.modalService
      .open(content, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static"
      })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  getDismissReason(reason: any): any {
    console.log(reason);
  }

  saveItem(data: ILayanan) {

    if(data.article!=null)
      data.idarticle=data.article.idarticle;
    this.spinner.show();
    this.agentService.saveNewLayanan(data).subscribe(result => {
      this.spinner.hide();
      if (data.idservice <= 0) {
        this.layanans.push(result);
        this.modalService.dismissAll();
      } else {
        this.modalService.dismissAll(data);
      }
      this.alertService.success("Success", "success");
    });
  }

  editItem(item, content) {
    this.form = this.agentService.createEditLayananForm(item);
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title", size: "lg" })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          if (reason && reason.idservice) {
            item.name = reason.name;
            item.price = reason.price;
            this.alertService.success(null, "success");
          }
        }
      );
  }
}
