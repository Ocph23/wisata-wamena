import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authentification/auth.service';
import { IAgent, ITransaction } from 'src/app/models/models.component';
import { AgentService } from 'src/app/services/agent.service';

@Component({
  selector: 'app-transaksi',
  templateUrl: './transaksi.component.html',
  styleUrls: ['./transaksi.component.scss']
})
export class TransaksiComponent implements OnInit {
  Data: ITransaction[];
  dataAgent: IAgent;

  constructor(private agentService: AgentService, private auth: AuthService) {}

  async ngOnInit() {
    this.dataAgent = await this.auth.getAgentProfile();
    this.Data = (await this.agentService.getTransaction(
      this.dataAgent.idagent
    )) as ITransaction[];
  }

  padNum(num, length) {
    return Array(length + 1 - num.toString().length).join('0') + num;
  }

  async confirm(item: ITransaction) {
    const isSuccess = await this.agentService.confirmPayment(item);
    if (isSuccess) {
        item.status = 'success';
    }
  }
}
