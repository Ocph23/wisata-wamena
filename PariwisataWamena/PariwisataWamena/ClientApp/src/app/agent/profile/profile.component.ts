import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { ActivatedRoute } from '@angular/router';
import { IModalResult, IAgent } from 'src/app/models/models.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/authentification/auth.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class AgentProfileComponent implements OnInit {
  tinymceInit: any;

  @ViewChild('swalMessage') swal: SwalComponent;
  form: FormGroup=null;
  private Data: IAgent;
  constructor(
    private alertService: AlertService,
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private http: HttpClient,
    private auth: AuthService,
    private spinner: NgxSpinnerService
  ) {
    this.tinymceInit = {
      height: 500,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste imagetools wordcount'
      ],
      // tslint:disable-next-line:max-line-length
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
      ],

      images_upload_handler: function (blobInfo, success, failure) {
        let xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'api/Image/upload');

        xhr.onload = function () {
          let json;

          if (xhr.status < 200 || xhr.status >= 300) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.imageUrl !== 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
          }

          success(json.imageUrl);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
      }

    };


    this.LoadProfile();
  }
  ngOnInit() {
    this.alertService.setSwal(this.swal);
  }

  async LoadProfile() {
    let data: IAgent;
    if (!this.Data) {
        data = await this.auth.getAgentProfile();
    } else {
    data = this.Data;
    }

    this.form = this.fb.group({
      'idagent': [data.idagent, Validators.required],
      'firstName': [data.name, Validators.required],
      'contactname': [data.contactName, Validators.required],
      'email': [data.email, Validators.required],
      'address': [data.address, Validators.required],
      'telepon': [data.telepon, Validators.required],
      'profile': [data.profile, Validators.required],
    });
  }


  save(data: IAgent) {
    this.spinner.show();
    this.http.put<IAgent>('api/agent/' + data.idagent, data, this.auth.getHttpHeader())
      .subscribe(x => {
        this.alertService.success(null, 'success');
        this.spinner.hide();
      }, error => {
        this.alertService.error('error', error);
        this.spinner.hide();
      });
  }
}
