import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ILayanan, ArticleType, IArticle } from 'src/app/models/models.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AgentService } from 'src/app/services/agent.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AlertService } from 'src/app/services/alert.service';
import { ArticleService } from 'src/app/services/article.service';


@Component({
  selector: 'app-add-layanan',
  templateUrl: './add-layanan.component.html',
  styleUrls: ['./add-layanan.component.css']
})

export class AddLayananComponent implements OnInit {

  @Input() form: FormGroup;
  @ViewChild('messageSwal') private messageSwal: SwalComponent;
  Destinasi: IArticle[];
  constructor(private agentService: AgentService,  private alertService: AlertService, private articleService: ArticleService,
    private router: ActivatedRoute,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.alertService.setSwal(this.messageSwal);

    this.articleService.getData(ArticleType.Destinasi).then(x => {
      this.Destinasi = x.datas;
    });

  }

  saveItem(data: ILayanan) {
    this.agentService.saveNewLayanan(data).subscribe(x => {
      const result = { success: true, data: data };
      this.activeModal.close(result);
      this.messageSwal.text = 'Success';
      this.messageSwal.show();
    });
  }

}
