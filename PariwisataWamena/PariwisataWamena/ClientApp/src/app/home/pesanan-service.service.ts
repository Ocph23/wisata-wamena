import { Injectable, Inject, ViewChild } from '@angular/core';
import {
  ILayanan,
  StorageHelper,
  ITransaction
} from '../models/models.component';

import { AuthService } from '../authentification/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PesananServiceService {
  public storage: StorageHelper.LocalStorageWorker = new StorageHelper.LocalStorageWorker();
  layanans: ILayanan[] = [];
  constructor(
    @Inject('BASE_URL') private baseUrl: string,
    private auth: AuthService,
    private http: HttpClient,
  ) {
    this.layanans = this.storage.getObject('layanans') as ILayanan[];
    if (this.layanans === undefined || this.layanans == null) {
      this.layanans = [];
    }
  }
  add(item: ILayanan) {
    const itemLayanan = this.layanans.find(x => x.idservice === item.idservice);
    if (itemLayanan === null || itemLayanan === undefined) {
      item.count = 1;
      this.layanans.push(item);
    } else {
      itemLayanan.count++;
    }

    this.storage.addObject('layanans', this.layanans);
  }

  delete(item: ILayanan) {
    this.layanans.splice(this.layanans.indexOf(item), 1);
    this.storage.addObject('layanans', this.layanans);
  }

  getTotal(): number {
    return this.layanans.map(x => x.price * x.count).reduce((x, y) => x + y, 0);
  }

  async createTransaction(data: ITransaction) {
    return await this.http.post<ITransaction>(
      this.baseUrl + 'api/service/transcation/create', data, this.auth.getHttpHeader()
    ).toPromise();
  }

  count(): number {
    return this.layanans.length;
  }
}
