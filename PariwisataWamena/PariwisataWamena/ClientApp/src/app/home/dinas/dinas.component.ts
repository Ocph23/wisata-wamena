import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { PanelArticle, IArticle, ArticleType } from 'src/app/models/models.component';

@Component({
  selector: 'app-dinas',
  templateUrl: './dinas.component.html',
  styleUrls: ['./dinas.component.scss']
})
export class DinasComponent implements OnInit {
  page: IArticle;

  Data: PanelArticle = { selected: null, datas: [] };
  notFound: IArticle = { title: 'Data Tidak Ditemukan' } as IArticle;
  Datas: IArticle[];

  constructor(private articleService: ArticleService) {
   this.articleService.getData(ArticleType.Dinas).then(x => {
    this.Data = x;
    this.onClickProfile();
    });

  }

 ngOnInit() {

  }

  onClickProfile() {
    this.view('profile');
  }


  onClickVisiMisi() {
    this.view('VISI DAN MISI');
  }


  onClickStruktur() {
    this.view('STRUKTUR');
  }


  onClickKontak() {
    this.view('contact');
  }



  private view(data: string) {
    const selected = this.Data.datas.find(x => x.title.toLowerCase() === data.toLowerCase());
    if (selected) {
      this.page = selected;
    } else {
      this.page = this.notFound;
    }
  }

}
