import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ILayanan, IAgent, IArticle } from 'src/app/models/models.component';
import { PesananServiceService } from '../pesanan-service.service';
import { AgentService } from 'src/app/services/agent.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { AlertService } from 'src/app/services/alert.service';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-home-layanan-detail',
  templateUrl: './home-layanan-detail.component.html',
  styleUrls: ['./home-layanan-detail.component.scss']
})
export class HomeLayananDetailComponent implements OnInit {
  @ViewChild('swalMessage') swal: SwalComponent;
  Data: ILayanan;
  DataAgent: IAgent[] = [];
  constructor(private agentService: AgentService, private alertService: AlertService ,
    private articleService:ArticleService,
     private router: Router, public pesanan: PesananServiceService) {
    if (agentService.LayananSelected === null || agentService.LayananSelected === undefined) {
      this.router.navigate(['home/agent']);
    }
    this.Data = agentService.LayananSelected as ILayanan;
    this.DataAgent.push(this.Data.agent);
  }

  ngOnInit() {
    this.alertService.setSwal(this.swal);
  }


  onSelectagent(item: IAgent) {
    this.agentService.Selected = item;
  }

  add(item: ILayanan) {
    this.pesanan.add(item);
    this.alertService.success('Success', 'Pesanan Berhasil Ditambahkan');
  }

  
  showDetail(data: IArticle) {
    this.articleService.currentArticle = data;
    this.router.navigate(['/home/detail']);
  }


}
