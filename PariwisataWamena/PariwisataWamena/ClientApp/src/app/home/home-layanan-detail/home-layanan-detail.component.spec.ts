import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeLayananDetailComponent } from './home-layanan-detail.component';

describe('HomeLayananDetailComponent', () => {
  let component: HomeLayananDetailComponent;
  let fixture: ComponentFixture<HomeLayananDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeLayananDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeLayananDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
