import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { IAgent, ILayanan, IArticle } from 'src/app/models/models.component';
import { PesananServiceService } from '../pesanan-service.service';
import { AgentService } from 'src/app/services/agent.service';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-home-agent',
  templateUrl: './home-agent.component.html',
  styleUrls: ['./home-agent.component.scss']
})
export class HomeAgentComponent implements OnInit {

  source: IAgent[];
  Data: IAgent[];
  constructor(public agentService: AgentService, private router: Router) {

   }

  ngOnInit() {
    this.agentService.get().then(x => {
      this.source = x;
      this.Data = this.Data = this.source.copyWithin(this.source.length, 1, this.source.length);
    });
  }



  onSearchHandle($event) {
    if ($event) {
      this.Data = this.source.filter(x => x.name.includes($event) || x.profile.includes($event));
    } else {
      this.Data = this.source.copyWithin(this.source.length, 1, this.source.length);
    }
  }

  onSelectagent(item: IAgent) {
    this.agentService.Selected = item;
  }


}

@Component({
  selector: 'app-home-agent-layanan',
  templateUrl: './home-agent-layanan.component.html',
  styleUrls: ['./home-agent-layanan.component.scss']
})
export class HomeAgentLayananComponent implements OnInit {
  source: ILayanan[];
  Data: ILayanan[];
  isActive: boolean;
  constructor(public agentService: AgentService, private router: Router, 
     private pesananService: PesananServiceService) {
   }

  ngOnInit() {
    this.agentService.getLayanan().then(x => {
      this.source = x;
      this.Data = this.Data = this.source.copyWithin(this.source.length, 1, this.source.length);
      this.isActive = true;
    });

  }

  onSearchHandle($event) {
    if ($event) {
      this.Data = this.source.filter(x => x.name.includes($event) || x.content.includes($event));
    } else {
      this.Data = this.source.copyWithin(this.source.length, 1, this.source.length);
    }
  }


  addPesanan(item: ILayanan) {
    this.pesananService.add(item);
  }

  onSelectLayanan(item: ILayanan) {
    this.agentService.LayananSelected = item;
  }

  

}

