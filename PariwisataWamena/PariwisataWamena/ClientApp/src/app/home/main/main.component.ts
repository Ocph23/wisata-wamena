import { Component, OnInit, Input, Injectable} from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { ArticleService } from '../../services/article.service';
import { DetailComponent } from '../detail/detail.component';
import { PanelArticle, ArticleType, IArticle } from 'src/app/models/models.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss', '../home.component.scss']
})


export class MainComponent implements OnInit {

  Kuliner: PanelArticle;
  Akomodasi: PanelArticle = { selected: null, datas: [] };
  Destinasi: PanelArticle = { selected: null, datas: [] };
  Datas: IPanelMain[] = [];
  images = ['assets/images/sliders/slide1.jpg', 'assets/images/sliders/slide2.jpg',
  'assets/images/sliders/slide3.jpg',
  'assets/images/sliders/slide4.jpg',
  'assets/images/sliders/slide5.jpg',
  'assets/images/sliders/slide6.png',
  'assets/images/sliders/slide7.png',
  'assets/images/sliders/slide8.jpg'];


  // [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);

  constructor(public dataService: ArticleService, private router: Router) {
    this.dataService.getData(ArticleType.Destinasi).then(x => {
      if (x.datas.length > 0) {
        x.datas = this.GetLimit(x.datas.filter(xArticle => xArticle.status === 'publish'), 3);
        this.Datas.push({ PanelType: ArticleType.Destinasi, title: ArticleType.Destinasi.toLocaleUpperCase(),
          Data: x, color1: 'blue', color2: 'blue' });

        this.dataService.getData(ArticleType.Akomodasi).then(y => {
          if (y.datas.length > 0) {
            y.datas = this.GetLimit(y.datas.filter(t => t.status === 'publish'), 3);
            this.Datas.push({
              PanelType: ArticleType.Akomodasi, title: ArticleType.Akomodasi.toLocaleUpperCase(),
              Data: y, color1: 'green', color2: 'green'
            });


            this.dataService.getData(ArticleType.Kuliner).then(z => {
              if (z.datas.length > 0) {
                z.datas = this.GetLimit(z.datas.filter(xArticle => xArticle.status === 'publish'), 3);
                this.Datas.push({
                  PanelType: ArticleType.Kuliner, title: ArticleType.Kuliner.toLocaleUpperCase(),
                  Data: z, color1: 'orange', color2: 'orange'
                });
              }
            });
          }
        });
      }
    });
   }

  async ngOnInit() {

  }

  GetLimit(datas: IArticle[], limit: number): IArticle[] {

    if (datas.length > limit) {
      datas.splice(limit, datas.length - limit);
    }

    return datas;


  }


}



@Component({
  selector: 'app-main-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./main.component.scss', '../home.component.scss']
})

export class MainPanelComponent implements OnInit {
  @Input() DataPanel: IPanelMain;
  constructor(private dataService: ArticleService, private router: Router) {
  }

  isActive: boolean;

  ngOnInit(): void {
    setTimeout(x => {
      this.isActive = this.isActive = true;
    }, 2000);
  }


  public onSelectArticle(data) {
    this.dataService.currentArticle = data;
    this.router.navigate(['/home/detail']);
  }
}


@Component({
  selector: 'app-berita-panel',
  templateUrl: './berita.component.html',
  styleUrls:   ['./berita.component.scss']
})

export class BeritaPanelComponent implements OnInit {
  DataPanel: PanelArticle;
  constructor(private dataService: ArticleService, private router: Router) {
    this.dataService.getData(ArticleType.Berita).then(x => {
      if (x.datas.length > 0) {
        x.datas = this.GetLimit(x.datas.filter(xArticle => xArticle.status === 'publish').sort((a, b) => +new Date(b.createdate) - +new Date(a.createdate)), 3);
        this.DataPanel = { selected: dataService.GetRandomArticle(x.datas), datas: x.datas };
        setTimeout(xx => {
          this.isActive = this.isActive = true;
        }, 2000);
      }
    });
  }

  isActive: boolean;

  ngOnInit(): void {

  }


  GetLimit(datas: IArticle[], limit: number): IArticle[] {

    if (datas.length > limit) {
      datas.splice(limit, datas.length - limit);
    }

    return datas;


  }
  public onSelectArticle(data) {
    this.dataService.currentArticle = data;
    this.router.navigate(['/home/detail']);
  }

}


export interface IPanelMain {
  PanelType: ArticleType;
  Data: PanelArticle;
  title: string;
  color1: string;
  color2: string;
}
