import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAgentDetailComponent } from './home-agent-detail.component';

describe('HomeAgentDetailComponent', () => {
  let component: HomeAgentDetailComponent;
  let fixture: ComponentFixture<HomeAgentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAgentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAgentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
