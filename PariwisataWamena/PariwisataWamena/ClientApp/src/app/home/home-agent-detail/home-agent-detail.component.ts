import { Component, OnInit } from '@angular/core';
import { IAgent } from 'src/app/models/models.component';
import { Router } from '@angular/router';
import { AgentService } from 'src/app/services/agent.service';

@Component({
  selector: 'app-home-agent-detail',
  templateUrl: './home-agent-detail.component.html',
  styleUrls: ['./home-agent-detail.component.scss']
})
export class HomeAgentDetailComponent implements OnInit {
  Data: IAgent;
  constructor(private agentService: AgentService, private router: Router) {
    if (agentService.Selected === null || agentService.Selected === undefined) {
    this.router.navigate(['home/agent']);
    }
    this.Data = agentService.Selected as IAgent;

  }

  ngOnInit() {

  }

}
