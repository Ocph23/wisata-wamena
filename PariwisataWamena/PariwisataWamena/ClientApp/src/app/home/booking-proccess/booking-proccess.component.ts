import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authentification/auth.service';
import { PesananServiceService } from '../pesanan-service.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ITransaction } from 'src/app/models/models.component';

@Component({
  selector: 'app-booking-proccess',
  templateUrl: './booking-proccess.component.html',
  styleUrls: ['./booking-proccess.component.scss']
})
export class BookingProccessComponent implements OnInit {
  transaction: ITransaction;
  constructor(
    public auth: AuthService,
    private router: Router,
     public pesanan: PesananServiceService,
     private fb: FormBuilder

  ) { }

  ngOnInit() {
  }

  async setuju() {

    const lay = this.pesanan.layanans[0];
    const t = await this.auth.getTourisProfile();
    const data = {idservice: lay.idservice, idtransaction: 0, status: 'none',
    tourist: t, start: new Date(), end: new Date(), count: lay.count, idtouris: t.idtouris} as ITransaction;

    this.pesanan.createTransaction(data)
    .then(x => {
      this.transaction = x;
      this.pesanan.layanans.forEach(x=>{
        this.pesanan.delete(x);
      });
    }, error => {
        throw Error('Tidak Tersimpan');
    });
  }

}
