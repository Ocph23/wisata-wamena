import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingProccessComponent } from './booking-proccess.component';

describe('BookingProccessComponent', () => {
  let component: BookingProccessComponent;
  let fixture: ComponentFixture<BookingProccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingProccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingProccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
