import { Component, OnInit } from '@angular/core';
import { SignalRService } from 'src/app/services/signal-r.service';
import { IStatistic } from 'src/app/models/models.component';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {

  data: IStatistic;
  constructor(public signalr: SignalRService) {
   }

  ngOnInit() {

  }

}
