import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { IArticle } from 'src/app/models/models.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailComponent implements OnInit {
  public Data: IArticle;
  constructor(private articleService: ArticleService, private router: Router) {}

  ngOnInit() {
    if (this.articleService.currentArticle === undefined) {
      this.router.navigate(['/home/main']);
    } else {
      this.Data = this.articleService.currentArticle;
    }


    var _Hasync = _Hasync || [];
    _Hasync.push(['Histats.start', '1,4265032,4,605,110,55,00011111']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    _Hasync.push(['Histats.framed_page', '']);
    (function () {
      var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
      hs.src = ('//s10.histats.com/js15_as.js');
      (document.getElementsByTagName('head')[0] || document.getElementById('body')[0]).appendChild(hs);
    })();









  }
}
