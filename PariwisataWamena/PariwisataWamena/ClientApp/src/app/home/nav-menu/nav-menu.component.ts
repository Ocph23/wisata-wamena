import { Component, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/authentification/auth.service';
import { Router } from '@angular/router';
import { IUser, ILayanan } from 'src/app/models/models.component';
import { PesananServiceService } from '../pesanan-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent {
  navbarOpen = false;
  User: IUser ;
  constructor(public auth: AuthService, private router: Router,
    public pesanan: PesananServiceService) {

  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}


@Component({
  selector: 'app-menu-pesanan',
  templateUrl: './menu-pesanan.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class PesananMenuComponent {


 message = '';
 private _success = new Subject<string>();
 private staticAlertClosed = true;
  navbarOpen = false;
  showLoginForm: boolean;
  loginForm: FormGroup;

  constructor(
    public auth: AuthService,
     private router: Router,
      public pesanan: PesananServiceService,
      private fb: FormBuilder) {

        this._success.subscribe((param) => {
          this.staticAlertClosed = false;
          this.message = param; });
        this._success.pipe(
          debounceTime(3000)
        ).subscribe(() => {
          this.message = null;
          this.staticAlertClosed = true;
        });

  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  logout() {
     this.auth.logout();
  }

  nextProccess() {
    if (this.auth.hasLogin() && this.auth.IsInRole('tourist')) {
      this.router.navigate(['home/booking']);
    } else {
      this.showLoginForm = true;
      this.loginForm = this.fb.group({
        'userName': [null, Validators.required],
        'password': [null, Validators.required]
      });
    }
  }


  login(item: any) {
      this.auth.login(item.userName, item.password)
      .subscribe(x => {
        this.auth.storage.addObject('user', x);
        this.showLoginForm = false;
      },
      error => {
        this._success.next(`${error.error.message}`);
      });
  }


  cancel() {
    this.showLoginForm = false;
  }








}
