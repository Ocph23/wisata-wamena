import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from 'src/app/authentification/auth.service';
import { Router } from '@angular/router';
import { ITouris, ITransaction, IPayment } from 'src/app/models/models.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgentService } from 'src/app/services/agent.service';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  touristData: ITouris;
  transactions: ITransaction[];

  constructor(
    private modalService: NgbModal,
    private auth: AuthService,
    private router: Router,
    public agentService: AgentService
  ) {
    if (!auth.hasLogin() || !auth.IsInRole('tourist')) {
      this.router.navigate(['/user/login']);
    }
  }

  async ngOnInit() {
    this.touristData = await this.auth.getTourisProfile();
    this.transactions = await this.auth.getTouristTransaction(this.touristData.idtouris);
  }

  open(item: ITransaction) {
    // tslint:disable-next-line: no-use-before-declare
    const modalRef = this.modalService.open(UserProfileModalComponent, { backdrop: 'static' });
    modalRef.componentInstance.Item = item;
    modalRef.result.then(x => {
      item.idtransaction = x.idtransaction;
      item.payment = x;
    });
  }


  async changeAvatar(obj) {
    obj.click();
  }

  async onChangeAvatar(event, output) {
    output.src = URL.createObjectURL(event.target.files[0]);
    const formData = new FormData();
    formData.append('file', output);
    this.auth.changeTouristAvatar(formData);
  }
}

@Component({
  selector: 'app-user-profile-modal',
  templateUrl: './user-profile-modal.component.html',

})

export class UserProfileModalComponent implements OnInit {
  @Input() Item: ITransaction;
  form: FormGroup;

  constructor(public modal: NgbActiveModal,
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string,
    private fb: FormBuilder, private agentService: AgentService) {

    this.form = this.fb.group({
      bank: ['', Validators.required],
      tanggal: [new Date(), Validators.required],
      memo: [''],
      file: ['', Validators.required],
      amount: [0, Validators.required],
      idpayment: [0],
    });
  }

  ngOnInit(): void {

  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('file').setValue(file);
    }
  }
  async savePayment(item) {
    const formData = new FormData();
    formData.append('file', this.form.get('file').value);
    const resultPath = await this.upload(this.Item.idtransaction, formData);
    if (resultPath) {
      const tanggal = new Date(item.tanggal.year, item.tanggal.month, item.tanggal.day);
      const data = {
        amount: item.amount, bank: item.bank, idpayment: item.idpayment, idtransaction: this.Item.idtransaction,
        datepayment: tanggal, memo: item.memo
      } as IPayment;
      data.photo = resultPath.imageUrl;
      data.idtransaction = this.Item.idtransaction;
      const result = await this.agentService.createPayment(data);
      if (result) {
        this.Item.status = 'Menunggu Konfirmasi';
        this.Item.payment = result;
      }
      this.modal.close(result);
    }

  }

  public async upload(idtransaction: number, data) {
    return await this.http.post<any>(this.baseUrl + 'api/image/UploadPayment?id=' + idtransaction, data).toPromise();
  }

}
