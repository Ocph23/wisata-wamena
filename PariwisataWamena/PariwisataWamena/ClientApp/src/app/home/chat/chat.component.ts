import { Component, OnInit } from '@angular/core';
import { IChat } from 'src/app/models/models.component';
import { SignalRService } from 'src/app/services/signal-r.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/authentification/auth.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  datas: IChat[];
  message: string;
  constructor(public signalRService: SignalRService, private http: HttpClient) {
    this.datas = signalRService.datas;
  }

  ngOnInit() {
    // this.signalRService.startConnection();
    // this.startHttpRequest();
  }

  sendMessage() {
    const chat = { pesan: this.message } as IChat;
    this.signalRService.post(chat);
    this.message = '';
  }


  public get Datas(): IChat[] {
   return this.signalRService.datas.sort((a, b) => <any>new Date(a.tanggal) - <any>new Date(b.tanggal.valueOf()));
  }


  Calculate(tgl: Date) {
    const duration = new Date().valueOf() - new Date(tgl).valueOf();
    let value = duration.valueOf() / 1000;
    const units = [
     {name: 'thn', value: 24 * 60 * 60 * 365},
     {name: 'bln', value: 24 * 60 * 60 * 30},
     {name: 'mng', value:  24 * 60 * 60 * 7},
     {name: 'hr', value:  24 * 60 * 60},
     {name: 'jm', value: 60 * 60},
     {name: 'mn', value:  60},
     {name: 's', value:  1}
    ];

    const result = [];

    units.forEach(obj => {
      const p =  Math.floor(value / obj.value);
      if (p === 1) { result.push(p + ' ' + obj.name); }
      if (p >= 2) { result.push(p + ' ' + obj.name); }
      value %= obj.value;
    });

    return result[0] + ' ' + result[1] + ' lalu';


  }
}
