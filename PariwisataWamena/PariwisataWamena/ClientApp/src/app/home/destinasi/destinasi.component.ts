import { Component, OnInit } from '@angular/core';
import { PanelArticle, ArticleType, IArticle } from 'src/app/models/models.component';
import { ArticleService } from '../../services/article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-destinasi',
  templateUrl: './destinasi.component.html',
  styleUrls: ['./destinasi.component.scss']
})
export class DestinasiComponent implements OnInit {

  public Data: PanelArticle = { selected: null, datas: [] };
  source: IArticle[];
  constructor(public articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.articleService.getData(ArticleType.Destinasi).then(x => {
      this.source = x.datas.filter( xc => xc.status === 'publish');
      this.Data = x;
      this.Data.datas = x.datas.filter( xc => xc.status === 'publish');
    });
  }


  showDetail(data: IArticle) {
    this.articleService.currentArticle = data;
    this.router.navigate(['/home/detail']);
  }


  onSearchHandle($event) {
    if ($event) {
      this.Data.datas = this.source.filter(x => x.title.includes($event) || x.content.includes($event));
    } else {
      this.Data.datas = this.source.copyWithin(this.source.length, 1, this.source.length);
    }
  }
}
