import { TestBed } from '@angular/core/testing';

import { PesananServiceService } from './pesanan-service.service';

describe('PesananServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PesananServiceService = TestBed.get(PesananServiceService);
    expect(service).toBeTruthy();
  });
});
