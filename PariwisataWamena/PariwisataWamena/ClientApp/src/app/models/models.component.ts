import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})
export class ModelsComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}

export interface IChangePassword {
  email: string;
  password: string;
  newpassword: string;
  confirmpassword: string;
}

export interface IStatistic {
  today: number;
  month: number;
  year: number;
  online: number;
}

export interface IChat {
  tanggal: Date | string;
  pesan: string;
  nama: string;
}

export interface IModalResult {
  success: boolean;
  data: any;
}

export interface PanelArticle {
  datas: IArticle[];
  selected: IArticle;
}

export interface IArticle {
  desc: string;
  idarticle: number;
  title: string;
  content: string;
  draft: string;
  type: string;
  createdate: Date | string;
  thumb: string;
  iduser: number;
  status: string;
  user: IUser;
}

export enum ArticleType {
  Kuliner = 'Kuliner',
  Akomodasi = 'Akomodasi',
  Destinasi = 'Destinasi',
  Dinas = 'Dinas',
  Berita = 'Berita'
}

export interface IUser {
  iduser: number;
  username: string;
  password: string;
  avatar: string;
  token: string;
  roles: IRole[];
  PasswordHash: string;
  PasswordSalt: string;
}

export interface IRole {
  idrole: number;
  name: string;
}

export interface IAgent {
  thumb: string;
  desc: string;

  idagent: number;

  name: string;

  email: string;

  address: string;

  contactName: string;

  telepon: string;

  profile: string;

  userid: number;

  Layanans: ILayanan[];

  Transactions: ITransaction[];
}

export interface ILayanan {
  thumb: string;
  desc: string;
  idservice: number;
idarticle:number;
  name: string;

  content: string;

  idagent: number;

  price: number;

  active: boolean;
  agent: IAgent;
  count: number;
  article:IArticle;

}

export interface ITransaction {
  tourist: ITouris;

  idtransaction: number;

  idservice: number;

  idtouris: number;

  count: number;

  start: Date | string;

  end: Date | string;

  status: string;

  payment: IPayment;

  layanan: ILayanan;

  agent: IAgent;
}

export interface ITouris {
  idtouris: number;

  nik: string;

  name: string;

  email: string;

  gender: string;

  address: string;

  telepon: string;

  iduser: number;
}

export interface IPayment {

  idpayment: number;

  idtransaction: number;

  bank: string;

  amount: number;

  datepayment: Date | string;

  memo: string;
  
  photo: string;

}

export module StorageHelper {
  export interface IStorageItem {
    key: string;
    value: any;
  }

  export class StorageItem {
    key: string;
    value: any;

    constructor(data: IStorageItem) {
      this.key = data.key;
      this.value = data.value;
    }
  }

  // class for working with local storage in browser (common that can use other classes for store some data)
  export class LocalStorageWorker {
    localStorageSupported: boolean;

    constructor() {
      this.localStorageSupported =
        typeof window['localStorage'] !== 'undefined' &&
        window['localStorage'] != null;
    }

    // add value to storage
    add(key: string, item: string) {
      if (this.localStorageSupported) {
        localStorage.setItem(key, item);
      }
    }

    addObject(key: string, data: any) {
      const jsonData = JSON.stringify(data);
      if (this.localStorageSupported) {
        localStorage.setItem(key, jsonData);
      }
    }

    getObject(key: string) {
      const item = localStorage.getItem(key);
      return JSON.parse(item);
    }

    // get all values from storage (all items)
    getAllItems(): Array<StorageItem> {
      const list = new Array<StorageItem>();

      for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        const value = localStorage.getItem(key);

        list.push(
          new StorageItem({
            key: key,
            value: value
          })
        );
      }

      return list;
    }

    // get only all values from localStorage
    getAllValues(): Array<any> {
      const list = new Array<any>();

      for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        const value = localStorage.getItem(key);

        list.push(value);
      }

      return list;
    }

    // get one item by key from storage
    get(key: string): string {
      if (this.localStorageSupported) {
        const item = localStorage.getItem(key);
        return item;
      } else {
        return null;
      }
    }

    // remove value from storage
    remove(key: string) {
      if (this.localStorageSupported) {
        localStorage.removeItem(key);
      }
    }

    // clear storage (remove all items from it)
    clear() {
      if (this.localStorageSupported) {
        localStorage.clear();
      }
    }
  }

  // custom class for store emails in local storage
  export class EmailStorage {
    storageWorker: LocalStorageWorker;

    // main key that use for store list of emails
    storageKey: string;

    // list of emails
    addresses: Array<string>;

    constructor(storageKey: string) {
      this.storageWorker = new LocalStorageWorker();

      this.storageKey = storageKey;

      this.addresses = new Array<string>();

      this.activate();
    }

    // activate custom storage for emails
    activate() {
      // this.clear();

      this.loadAll();
    }

    // load all emails from storage to list for working with it
    loadAll() {
      const storageData = this.storageWorker.get(this.storageKey);

      if (storageData != null && storageData.length > 0) {
        const emails = JSON.parse(storageData);
        console.log(emails);
        if (emails != null) {
          this.addresses = emails;
        }
        console.log(this.addresses);
      }
    }

    // add new email (without duplicate)
    addEmail(email: string) {
      if (email.length > 0) {
        // 1. Split email addresses if needed (if we get list of emails)
        const mas = email.split(/,|;/g);
        // console.log(mas);

        // 2. Add each email in the splited list
        for (let i = 0; i < mas.length; i++) {
          // check if not exist and not add new (duplicate)
          const index = this.addresses.indexOf(mas[i].trim());
          if (index < 0) {
            this.addresses.push(mas[i].trim());
          }
        }

        console.log(this.addresses);

        // 3. save to storage
        this.save();
      }
    }

    // clear all data about emails
    clear() {
      // remove data by key from storage
      this.storageWorker.add(this.storageKey, '');

      // or remove with key
      // this.storageWorker.remove(this.storageKey);
    }

    // save to storage (save as JSON string)
    save() {
      const jsonEmails = JSON.stringify(this.addresses);
      this.storageWorker.add(this.storageKey, jsonEmails);
    }
  }
}
