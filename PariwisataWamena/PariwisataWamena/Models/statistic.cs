using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ocph.DAL;

namespace PariwisataWamena.Models {
    [TableName ("statistic")]
    public class statistic {
        [PrimaryKey ("id")]
        [DbColumn ("id")]
        public int id { get; set; }

        [DbColumn ("tanggal")]
        public DateTime Tanggal { get; set; }

        [DbColumn ("ip")]
        public string IpAddress { get; set; }

    }
}