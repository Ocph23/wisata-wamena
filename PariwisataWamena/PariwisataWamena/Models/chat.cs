using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ocph.DAL;

namespace PariwisataWamena.Models {
    [TableName ("pesan")]
    public class chat {
        [PrimaryKey ("id")]
        [DbColumn ("id")]
        public int id { get; set; }

        [DbColumn ("nama")]
        public string nama { get; set; }

        [DbColumn ("pesan")]
        public string pesan { get; set; }

        [DbColumn ("tanggal")]
        public DateTime tanggal { get; set; }

    }
}