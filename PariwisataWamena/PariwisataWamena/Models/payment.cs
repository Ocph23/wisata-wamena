﻿using Ocph.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PariwisataWamena.Models
{
    [TableName("payment")]
    public class payment
    {
        [PrimaryKey("idpayment")]
        [DbColumn("idpayment")]
        public int idpayment { get; set; }

        [DbColumn("idtransaction")]
        public int idtransaction { get; set; }


        [DbColumn("bank")]
        public string bank { get; set; }


        [DbColumn("amount")]
        public double amount { get; set; }


        [DbColumn("datepayment")]
        public DateTime datepayment { get; set; }
        

        [DbColumn("memo")]
        public string memo { get; set; }

         [DbColumn("photo")]
         public string photo { get; set; }

    }
}
