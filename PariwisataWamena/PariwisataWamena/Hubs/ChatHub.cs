using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
public class ChatHub : Hub {
    public async Task SendMessage (string user, string message) {
        await Clients.All.SendAsync ("ReceiveMessage", user, message);
    }

     public override  Task OnConnectedAsync()
    {
        UserHandler.ConnectedIds.Add(Context.ConnectionId);
         Clients.All.SendAsync ("OnConnect", UserHandler.ConnectedIds.Count);
        return base.OnConnectedAsync();
    }

    public override Task OnDisconnectedAsync(System.Exception exception){
        var id=UserHandler.ConnectedIds.FirstOrDefault();
        if(id!=null)
        UserHandler.ConnectedIds.Remove(id);
          Clients.All.SendAsync ("OnDisonnect", UserHandler.ConnectedIds.Count);
        return base.OnDisconnectedAsync(exception);
    }
}


public static class UserHandler
{
    public static HashSet<string> ConnectedIds = new HashSet<string>();
    
}

